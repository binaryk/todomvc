module.exports = {
    entry: "./src/index.tsx",

    resolve: {
        extensions: ['', '.js', '.jsx', '.ts', '.tsx', '.web.js', '.webpack.js']
    },


    output: {
        filename: 'bundle.js',
        path: __dirname + '/assets'
    },

    module: {
        loaders: [
            {
                test: /\.[j|t]sx?$/,
                loader: 'awesome-typescript-loader'
            }
        ]
    },

    devServer: {
        publicPath: '/assets',
        filename: 'bundle.js',
        port: 8080
    }

};