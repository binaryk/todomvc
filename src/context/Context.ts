import InitialState from '../api/todos';
import * as Immutable from 'immutable';
import * as Cursor from 'immutable/contrib/cursor';
import * as Rx from 'rx';


class Context{
    context: any;
    subject: any;

    constructor(){
        const immutableState = Immutable.fromJS(InitialState);
        this.context         = Cursor.from(immutableState, this.cursorIsUpdated.bind(this));
        this.subject         = new Rx.BehaviorSubject(this.context);
    }

    cursorIsUpdated(actualImmutableData){
        this.context = Cursor.from(actualImmutableData, this.cursorIsUpdated.bind(this));
        this.subject.onNext(this.context);
    }

    subscribeMePlease( callMeOnChange ){
        this.subject.subscribe(callMeOnChange);
    }

}

export default new Context;