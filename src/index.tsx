import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Context from './context/Context';
import Form from './components/Form';



const Item = (props) => {
    const {todo} = props;
    return (
        <li onClick={props.edit.bind({}, todo, 199)} className="ui-state-default">
            {todo.title}
        </li>
    )

}



class TodoMvc extends React.Component<any, any>{
    constructor() {
        super();
    }

    componentWillMount() {
        Context.subscribeMePlease(this.contextDidUpdated.bind(this))
    }

    contextDidUpdated(newGlobalCursor){
        console.log('new cursor index', newGlobalCursor.toJS());
        this.setState({
            todos: newGlobalCursor.get('list').toJS()
        });
    }

    editItem(todo){
        console.log(todo);
    }


    render(){
        console.log(this.state.todos);
        const items = this.state.todos.map( (el, idx) => {
            return <Item
                todo={el}
                key={idx}
                edit={this.editItem.bind(this)}
            />
        });

        return (
            <div>
              <div className="col-md-6">
                  Hello TodoMvc

                  <div className="container todolist not-done">
                      <h1>My todo list</h1>

                      <Form/>

                      {items}

                      <ul className="list-unstyled">
                      </ul>
                  </div>
              </div>

            </div>
        )
    }
}

const mountNode = document.getElementById('app');

ReactDOM.render(
    <TodoMvc/>,
    mountNode
);

