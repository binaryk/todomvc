import * as React from 'react';
import Context from '../context/Context';
import * as Immutable from 'immutable';


export default class Form extends React.Component<any, any>{
    subscription: any;
    constructor(){
        super();
    }

    componentWillMount() {
        Context.subscribeMePlease(this.contextWillUpdated.bind(this))
    }

    contextWillUpdated(newGlobalCursor){
        const todos = newGlobalCursor.get('list');
        this.setState({
            todos: todos.toJS()
        });

    }

    updateGlobalContext(newTodoString){
        console.log('my state is',this.state);
        const newTodo = {
            title: newTodoString,
            completed: false
        };
        const {todos} = this.state;
        todos.push(newTodo);

        console.log('todos', todos);
        Context.context.set('list', Immutable.fromJS(todos))
    }

    keyDown(event){

        const code = event.keyCode;
        if(code === 13){
            /*enter*/
            console.log(event.target.value);
            const newTodoString = event.target.value;
            this.updateGlobalContext(newTodoString);
            event.target.value = '';

        }
    }

    render(){
        console.log('first list', this.state);
        return (
            <div className="form-group">
                <input type="text" className="form-control"
                       onKeyDown={this.keyDown.bind(this)}
                />
            </div>
        )
    }
}